# Test Exercise

This is the test exercise made by [Eldar Avatov](https://gitlab.com/deadalice).

LIVE DEMO: https://alpha.deadalice.xyz/

## Description

This is a simple web application to demonstrate how easy dynamic fields can be added to a `User` (or any other) entity. It allows to initiate field schema for a `Tenant` and then add pre-defined fields to a `User` entity.

There is usually a 2 approaches to do this:

1. **Reference tables** - pretty easy to implement, we only need to define a dictionary table linked to a `Tenant` and a table with values linked to a `User`. The main disadvantage is that we need to join tables to get the values.
2. **JSONB column** - we can store schema and values in a corresponding JSONB columns. This approach is more flexible and allows to store any kind of data. Of course, we need to care about the schema validation and value actualization.

I've chosen the **second approach** because I really hate to choose easy ways. Ok, I'm joking. I've chosen it because it's more flexible and allows to store any kind of data. And the "care about things" part can be covered with modern day solutions like JSON Schema.

## Getting Started

The only thing you need to do is to clone the repository, copy `.env.sample` to `.env` file and edit it to match your db environment. Then run `rake db:setup` to create and seed the database.

Root points to a list of `Users`, you can choose and edit them to see how dynamic fields work. Everything works, yay! You can use 4 kinds of fields: `string`, `integer`, `single_select` and `multi_select` all the way you want.

Too add or remove fields you need to use console. See [Seeds](./db/seeds.rb) for examples, [Tenant Concern](./app/models/concerns/schema_buildable.rb) or [Model Concern](./app/models/concerns/schema_attributable.rb) for implementation examples and helper methods. Also, check [Specs](./spec/) for more examples.

## Methods

### Tenant

1. `add_string_attribute` - adds a string field to a schema.
2. `add_integer_attribute` - adds an integer field to a schema.
3. `add_single_select_attribute` - adds a single select field to a schema.
4. `add_multi_select_attribute` - adds a multi select field to a schema.
5. `remove_attribute` - removes a field from a schema.

All the `add_*` method calls will also update corresponding schema key. You can use `options` hash to add ANY kind of subkeys allowed by JSON Schema. Also, you can use `required: true` option to make a field required. For a `User` without a required field you will see a validation error.

### User

1. `schema_attributes` - returns a list of schema attributes.
2. `schema_attribute_type` - returns a type of a schema attribute.
3. `schema_enum_values` - returns a list of enum values for a schema attribute.
4. `list_schema_attributes` - returns a list of schema attributes with values e.g. for a console output.

Also, `User` responds to all the schema attributes as methods. You can use them to get or set values for a field.

## Considerations

1. We have only one `Tenant` for now. We can add new tenants only using console.
2. Field `required` allows to store `null` values! We assume that the field can be not initialized for a new `User` instance. If you want to make a field required, it can be excluded from schema partials and will be validated by the model as a regular JSONB field key.

## Caveats

This is the best part!

1. **Performance** - JSONB columns use GIN indexes with `jsonb_path_ops` operator class. Assuming, we will never have need to sort those values, it's a good choice for a fairly fast search and filter. Of course, validation uses schema field a lot and this will affect the performance. Field delegation with partial/covering indexes can be a good solution for this.
2. **Validation** - I've used JSON Schema with both schema and values. OMG, I love it so much! It allows us store any kind of values in a separate definitions, it also cares about both schema and values validation and allows us to add any possible new field types in no time. We also can extend ANY existing field creating different type of contraints, like `minLength`, `maxLength`, `pattern`, `enum` and so on. We even can validate values with regex by adding only one line to the schema file or just use it as option in a field adding method!
3. **Extensibility** - Well, not really for now. I've hardcoded `Tenant` into concerns so we always expect to have a `Tenant` entity. But we can make this an attribute for a model! We just need to set it before we will use any of the of key methods. E.g. we can set this in a local variable or in a `before_action` callback in a controller. We also can make a `Tenant` a polymorphic entity of other entities, so we could just add this feature to any entity as one-to-one relation.

## References

1. [JSON Schema](https://json-schema.org/)
2. [PostgreSQL JSONB](https://www.postgresql.org/docs/9.4/datatype-json.html)
3. [PostgreSQL JSONB Indexing](https://www.postgresql.org/docs/9.4/datatype-json.html#JSON-INDEXING)
