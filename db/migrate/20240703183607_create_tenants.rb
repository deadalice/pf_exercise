# frozen_string_literal: true

class CreateTenants < ActiveRecord::Migration[7.0]
  def change
    create_table :tenants do |t|
      t.jsonb :schema, default: JSON.parse(File.read(Rails.root.join('schema/base.json'))), null: false

      t.timestamps
    end

    # Add index to schema column, array 'properties', using jsonb_path_ops for faster queries.
    # Let's be honest, I'm not sure this is the best way to index a JSONB array column.
    execute <<-SQL.squish
      CREATE INDEX index_tenants_on_schema_properties ON tenants USING gin ((schema -> 'properties') jsonb_path_ops);
    SQL
  end
end
