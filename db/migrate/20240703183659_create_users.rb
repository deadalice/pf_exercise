class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.references :tenant, null: false, foreign_key: true

      t.string :name, null: false

      t.jsonb :schema_data, default: {}

      t.timestamps
    end

    execute <<-SQL.squish
      CREATE INDEX index_users_on_schema_data ON users USING gin (schema_data jsonb_path_ops);
    SQL
  end
end
