# frozen_string_literal: true

# Let's create a new tenant with a schema that has a string attribute
tenant = Tenant.create!

tenant.add_string_attribute("first_name", required: true)
tenant.add_string_attribute("second_name")
tenant.add_integer_attribute("age", required: true)
tenant.add_single_select_attribute("sex", enum: %i[m f], required: true)
tenant.add_multi_select_attribute("size", items: { enum: %i[xs s m l xl] }, required: true)
tenant.save!

# Let's create a new user for the tenant
user = tenant.users.create!(name: "John Doe")
user.first_name = "Jane"
user.age = 25
user.sex = :f
user.size = %i[s m l]
user.save!
