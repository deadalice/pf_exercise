# frozen_string_literal: true

# If changing this file, make sure to update action_cable too
Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    origins "*"
    resource "*",
             headers: :any,
             methods: %i[get post patch put delete options head],
             expose: %w[Authorization Refresh-Token]
  end
end
