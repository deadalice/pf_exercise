require 'rails_helper'

RSpec.describe User, type: :model do
  let!(:tenant) { Tenant.create! }
  let!(:user) { User.create!(tenant: tenant, name: "Test User") }

  before do
    tenant.add_string_attribute("first_name", required: true)
    tenant.add_string_attribute("second_name", required: false)
    tenant.add_integer_attribute("age", required: true)
    tenant.add_single_select_attribute("sex", enum: %i[m f], required: true)
    tenant.add_multi_select_attribute("size", items: { enum: %i[xs s m l xl] }, required: true)
    tenant.save!
  end

  context "when manipulating a user" do
    it "creates, updates, and validates the user" do
      expect(user).to be_persisted

      user.first_name = "Jane"
      user.age = 25
      user.sex = :f
      user.size = %i[s m l]
      user.save!

      expect(user.first_name).to eq("Jane")
      expect(user.age).to eq(25)
      expect(user.sex).to eq("f")
      expect(user.size).to eq(%w[s m l])

      tenant.remove_schema_attribute("first_name")

      expect(user).to be_valid
      expect { user.first_name }.to raise_error(NoMethodError)

      user.sex = "x"
      user.size = %w[xxl]

      expect(user).not_to be_valid
    end
  end
end
