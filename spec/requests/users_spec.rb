require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  let!(:tenant) { Tenant.create! }
  let!(:user) { User.create!(tenant: tenant, name: "Test User") }

  before do
    tenant.add_string_attribute("first_name", required: true)
    tenant.add_string_attribute("second_name", required: false)
    tenant.add_integer_attribute("age", required: true)
    tenant.add_single_select_attribute("sex", enum: %i[m f], required: true)
    tenant.add_multi_select_attribute("size", items: { enum: %i[xs s m l xl] }, required: true)
    tenant.save!
  end

  describe "GET #index" do
    it "returns a success response" do
      get :index

      expect(response).to be_successful
    end
  end

  describe "PUT #update" do
    it "updates the requested user" do
      new_attributes = { first_name: "Updated User", sex: :f, size: %i[s m l], age: 31 }
      put :update, params: { id: user.id, user: { schema_data: new_attributes } }

      expect(response).to redirect_to(users_path)

      user.reload

      expect(user.first_name).to eq("Updated User")
      expect(user.sex).to eq("f")
      expect(user.size).to eq(%w[s m l])
      expect(user.age).to eq(31)
    end
  end
end
