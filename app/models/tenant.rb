# frozen_string_literal: true

# == Schema Information
#
# Table name: tenants
#
#  id         :bigint           not null, primary key
#  schema     :jsonb            not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_tenants_on_schema_properties  (((schema -> 'properties'::text)) jsonb_path_ops) USING gin
#
class Tenant < ApplicationRecord
  include SchemaBuildable

  has_many :users, dependent: :destroy
end
