# frozen_string_literal: true

module SchemaAttributable
  extend ActiveSupport::Concern

  included do
    before_validation :initialize_schema_data, on: :create
    before_validation :clean_up_schema_data
    before_validation :validate_schema_data

    # Use missing_method to define getters and setters for schema_data,
    # based on the schema defined in the tenant. If the schema defines a
    # string attribute called "name", we can use user.name to get the value
    # and user.name = "John Doe" to set the value. Raises an error if the
    # attribute is not defined in the schema.
    def method_missing(method_name, *args, &)
      method_name_string = method_name.to_s
      if method_name_string.end_with?("=")
        attribute = method_name_string[0...-1]
        if tenant.schema["properties"].key?(attribute)
          schema_data[attribute] = args.first
        else
          super
        end
      elsif tenant.schema["properties"].key?(method_name_string)
        schema_data[method_name_string]
      else
        super
      end
    end

    def respond_to_missing?(method_name, include_private = false)
      tenant.schema["properties"].key?(method_name.to_s.delete("=")) || super
    end
  end

  # Helper method to convert a string value to the correct type based on the schema.
  # I guess, much better could be converting based on the schema values,
  # but it is what it is.
  def convert_string_to_schema_value(key, value)
    case tenant.schema["properties"][key]["title"]
    when "Integer"
      # Well, strong conversion to integer could be better idea for a real world.
      value.to_i
    when "MultiSelect"
      # Dirty hack to reject unwanted empty string added by the form.
      value.reject(&:empty?)
    else
      value
    end
  end

  # Returns a list of schema attributes e.g. for console
  # user.schema_attributes
  # => ["first_name", "second_name", "age"]
  def schema_attributes
    tenant.schema["properties"].keys
  end

  # Returns the type of a schema attribute e.g. for console
  # user.schema_attribute_type("first_name")
  # => "string"
  def schema_attribute_type(attribute)
    tenant.schema["properties"][attribute.to_s]["title"]
  end

  # Helper method to get the enum values of a schema attribute
  def schema_enum_values(attribute)
    tenant.schema["properties"][attribute.to_s]["enum"] ||
      tenant.schema["properties"][attribute.to_s]["items"]["enum"]
  end

  # Returns a list of schema attributes and their types e.g. for console
  # user.list_schema_attributes
  # => [["first_name", "string"], ["second_name", "string"], ["age", "integer"]
  def list_schema_attributes
    schema_attributes.map do |attribute|
      [attribute, schema_attribute_type(attribute)]
    end
  end

  def schema_field_required?(attribute)
    tenant.schema["required"].include?(attribute)
  end

  private

  # Initialize schema data with nil values for all required attributes
  def initialize_schema_data
    self.schema_data =
      tenant.schema["properties"].each_with_object({}) do |(name, _), hash|
        hash[name] = nil if tenant.schema["required"].include?(name)
      end
  end

  # Remove value if tenant schema does not define the attribute anymore
  def clean_up_schema_data
    schema_data.each_key do |key|
      schema_data.delete(key) unless tenant.schema["properties"].key?(key)
    end
  end

  # Validate schema data against the tenant schema using JSON Schema
  def validate_schema_data
    JSON::Validator.fully_validate(tenant.schema, schema_data).each do |error|
      # Extract error message from JSON Schema error message
      error = error.split(" in schema").first

      errors.add(:schema_data, error)
    end
  end
end
