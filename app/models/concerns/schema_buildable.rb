# frozen_string_literal: true

module SchemaBuildable
  extend ActiveSupport::Concern

  # Adds a new string attribute to the schema
  def add_string_attribute(name, options = {})
    add_schema_attribute(name, "string", options)
  end

  # Adds a new integer attribute to the schema
  def add_integer_attribute(name, options = {})
    add_schema_attribute(name, "integer", options)
  end

  # Adds a new single select attribute to the schema
  def add_single_select_attribute(name, options = {})
    # Add nil to enum if present
    options = options.with_indifferent_access
    options[:enum] = [nil] + options[:enum] if options[:enum]

    add_schema_attribute(name, "single_select", options)
  end

  # Adds a new multiple select attribute to the schema
  def add_multi_select_attribute(name, options = {})
    # Add nil to enum if present
    options = options.with_indifferent_access
    options[:enum] = [nil] + options[:enum] if options[:enum]

    add_schema_attribute(name, "multi_select", options)
  end

  def remove_schema_attribute(name)
    schema["properties"].delete(name)
    sync_required_attributes
  end

  private

  # Adds a new generic attribute to the schema. Options can contain `required: true`
  # to mark the attribute as required. Options can be possibly extended with params
  # like `null: false`, `default: "John Doe"`, length: { minimum: 5, maximum: 10 } etc.
  def add_schema_attribute(name, type, options)
    required = options.delete(:required) || false

    schema["properties"][name] = JSON.parse(Rails.root.join("schema/#{type}.partial.json").read).merge(options)

    schema["required"] << name if required
    sync_required_attributes
  end

  # Clears required attributes if they are not present in the schema
  def sync_required_attributes
    schema["required"] = schema["required"] & schema["properties"].keys
    schema["required"].uniq!
  end
end
