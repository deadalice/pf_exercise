# frozen_string_literal: true

source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "3.3.0"

# Bundle edge Rails instead: gem "rails", github: "rails/rails", branch: "main"
gem "rails", "~> 7.0.8", ">= 7.0.8.4"

# The original asset pipeline for Rails [https://github.com/rails/sprockets-rails]
gem "sprockets-rails"

# Use postgresql as the database for Active Record
gem "pg", "~> 1.1"

# Use the Puma web server [https://github.com/puma/puma]
gem "puma", "~> 5.0"

# Bundle and transpile JavaScript [https://github.com/rails/jsbundling-rails]
gem "jsbundling-rails"

# Hotwire's SPA-like page accelerator [https://turbo.hotwired.dev]
gem "turbo-rails"

# Hotwire's modest JavaScript framework [https://stimulus.hotwired.dev]
gem "stimulus-rails"

# Bundle and process CSS [https://github.com/rails/cssbundling-rails]
gem "cssbundling-rails"

# Build JSON APIs with ease [https://github.com/rails/jbuilder]
gem "jbuilder"

# Use Redis adapter to run Action Cable in production
gem "redis", "~> 4.0"

# Use Kredis to get higher-level data types in Redis [https://github.com/rails/kredis]
# gem "kredis"

# Use Active Model has_secure_password [https://guides.rubyonrails.org/active_model_basics.html#securepassword]
# gem "bcrypt", "~> 3.1.7"

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem "tzinfo-data", platforms: %i[mingw mswin x64_mingw jruby]

# Reduces boot times through caching; required in config/boot.rb
gem "bootsnap", require: false

# Autoload dotenv in Rails. [https://github.com/bkeepers/dotenv]
gem "dotenv-rails"

# JSON schema validator
gem "json-schema"

# Use Sass to process CSS
# gem "sassc-rails"

# Use Active Storage variants [https://guides.rubyonrails.org/active_storage_overview.html#transforming-images]
# gem "image_processing", "~> 1.2"

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem "rack-cors"

group :development, :test do
  # See https://guides.rubyonrails.org/debugging_rails_applications.html#debugging-with-the-debug-gem
  gem "debug", platforms: %i[mri mingw x64_mingw]

  # RSpec for Rails [https://github.com/rspec/rspec-rails]
  gem "rspec-rails"
  # Provides integration between factory_bot and rails 5.0 or newer [https://github.com/thoughtbot/factory_bot_rails]
  gem "factory_bot_rails", require: false
  # Faker is a library for generating fake data such as names, addresses, and phone numbers
  gem "faker"
  # Bullet helps you kill N+1 queries and unused eager loading
  gem "bullet"
  # Automatic Ruby code style checking tool. [https://github.com/rubocop/rubocop]
  gem "rubocop", require: false
  # Automatic performance checking tool for Ruby code. [https://github.com/rubocop/rubocop-performance]
  gem "rubocop-performance", require: false
  # Automatic Rails code style checking tool. [https://github.com/rubocop/rubocop-rails]
  gem "rubocop-rails", require: false
  # Code style checking for RSpec files [https://github.com/rubocop/rubocop-rspec]
  gem "rubocop-rspec", require: false
  # Better Errors replaces the standard Rails error page with a much better
  gem "better_errors"
  # Retrieve the binding of a method's caller in MRI
  gem "binding_of_caller"
  # Annotate Rails classes with schema and routes info [https://github.com/ctran/annotate_models]
  gem "annotate"
end

group :development do
  # Use console on exceptions pages [https://github.com/rails/web-console]
  gem "web-console"

  # A Ruby language server for the Ruby language
  gem "solargraph"
  # Ruby source code analyzer
  gem "rufo"

  # Add speed badges [https://github.com/MiniProfiler/rack-mini-profiler]
  # gem "rack-mini-profiler"

  # Speed up commands on slow machines / big apps [https://github.com/rails/spring]
  # gem "spring"
end

group :test do
  # Use system testing [https://guides.rubyonrails.org/testing.html#system-testing]
  gem "capybara"
  gem "selenium-webdriver"
end
